var gulp = require('gulp');
var sass = require('gulp-ruby-sass');
var nano = require('gulp-cssnano');
var rename = require('gulp-rename');
var notify = require('gulp-notify');
var uglify = require('gulp-uglify');

// Sass
gulp.task('sass', function() {
	return sass('assets/scss/style.scss', { style: 'expanded' })
		//.pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
		.pipe(gulp.dest('assets/css'))
		.pipe(rename({suffix: '.min'}))
		.pipe(nano())
		.pipe(gulp.dest('assets/css'));
});

// Minfy JS
gulp.task('compress', function(){

	return gulp.src('assets/js/scripts.js')
		.pipe(uglify())
		.pipe(rename({suffix:'.min'}))
		.pipe(gulp.dest('assets/js'))
})

//Watch task
gulp.task('default',function() {
    gulp.watch('assets/scss/style.scss',['sass']);
    gulp.watch('assets/js/scripts.js',['compress']);
});