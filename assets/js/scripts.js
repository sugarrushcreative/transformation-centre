
// Contact Form Validation

$('#contact .form').submit(function(){

	var name = $('#input-name').val();
	var email = $('#input-email').val();
	var message = $('#input-message').val();

	// Clear previous errors
	$('.input-group').removeClass('error');

	function setError(message,id){

		var $el = $(id).parent();
		$el.addClass('error');
		$el.find('.error-text').html(message);
	}

	// Validation
	if(name==''){

		setError('Please enter a valid name', '#input-name');
		return false;
	}
	else if(email==''){

		setError('Please enter a valid email', '#input-email');
		return false;
	}
	else if(message==''){

		setError('Please enter a message', '#input-message');
		return false;
	}

	return true;
});

// Join Us Validation

$('#join-us .form').submit(function(){

	var validation = true;
	var scrolled = false;
	var name = $('#input-name').val();
	var email = $('#input-email').val();

	// Clear previous errors
	$('.input-group').removeClass('error');

	function setError(message,id){

		var $el = $(id).parent();
		$el.addClass('error');
		$el.find('.error-text').html(message);
		validation = false;

		if(!scrolled){ // Scroll to errored element

			var inputOffset = $el.offset().top;
			var bodyOffset = $('body').scrollTop();

			if(bodyOffset > inputOffset){
				console.log('yes');
				$('html, body').animate({ scrollTop: inputOffset-40 });
				scrolled = true;
			}
		}
	}

	// Validation
	if(name==''){

		setError('Please enter a valid name', '#input-name');
	}
	if(email==''){

		setError('Please enter a valid email', '#input-email');
	}

	return validation;
});

// Newsletter Validation

$('#newsletter-signup-form').submit(function(e){

	var fname = $('#mce-FNAME').val();
	var lname = $('#mce-LNAME').val();
	var email = $('#mce-EMAIL').val();

	// Clear previous errors
	$('.input-group').removeClass('error');

	function setError(message,id){

		var $el = $(id).parent();
		$el.addClass('error');
		$('.newsletter-error-text').html(message).css('display','block');
	}

	// Validation
	if(fname==''){

		setError('Please enter your first name', '#mce-FNAME');
		return false;
	}
	else if(lname==''){

		setError('Please enter your last name', '#mce-LNAME');
		return false;
	}
	else if(email==''){

		setError('Please enter a valid email', '#mce-EMAIL');
		return false;
	}

	return true;
});

// Mobile Navigation

$('.mobile-nav .hamburger').on('click', function(){

	$('body').toggleClass('nav-open');
	$('body').toggleClass('noscroll');
});

// Dropdown, touch support

var supportsTouch = 'ontouchstart' in window || navigator.msMaxTouchPoints;

if(supportsTouch){

	$('.dropdown').addClass('no-hover');

	$('.dropdown-container').on('click', function(){
		console.log('click');
		$('.dropdown').toggleClass('open');
	});
}

// Associates

window.onload = function(){

	var totalWidth = 0;
	var divWidth = parseInt($('.associates-carousel').css('width'));

	$('.associates-carousel').children('.associate-logo').each(function(){

		var width = parseInt($(this).css('width')) + 40;
		totalWidth += width;
	});

	// If logo's break onto another line, use caraousel
	if(totalWidth > divWidth){

		$('.associates-carousel').slick({

			dots: false,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			centerMode: true,
			variableWidth: true
		});
	}
}


// Stick navigation

var detachNav = false;
var stickNav = false;

window.onscroll = function(){

	var scrollTop = document.body.scrollTop;
	var headerHeight = parseInt($('header').css('height'));

	if(!detachNav && (scrollTop > 110) ){

		detachNav = true;
		$('header nav').addClass('detach');

		setTimeout(function(){ $('header nav').addClass('animate'); },100)
	}
	if(detachNav && (scrollTop <= 110) ){

		detachNav = false;
		$('header nav').removeClass('detach').removeClass('animate');
	}

	// Fix nav to page
	if(!stickNav && (scrollTop > headerHeight)){

		stickNav = true;
		$('header nav').addClass('fixed');
	}

	// Unfix nav
	if(stickNav && (scrollTop <= headerHeight)){

		stickNav = false;
		$('header nav').removeClass('fixed');
	}
}

// Phrase caraousel

$('.phrase-box').slick({

	dots: true,
	arrows: false,
	autoplay: true,
	autoplaySpeed: 4000,
	swipeToSlide: true
});


// Tabs

$('.tabs .tab').on('click',function(){

	var group = $(this).attr('data-tab');

	window.location.hash = group;

	// Switch Tab
	$('.tab.active').removeClass('active');
	$(this).addClass('active');

	// Switch Group
	$('.tab-group').removeClass('open');
	$('.tab-group.'+group).addClass('open');
});

var tab = window.location.hash;

if(tab){

	tab = tab.split('#')[1]; // remove hash;

	// Switch Tab
	$('.tab.active').removeClass('active');
	$('.tab[data-tab="'+tab+'"]').addClass('active');

	// Switch Group
	$('.tab-group').removeClass('open');
	$('.tab-group.'+tab).addClass('open');
}

// FAQ section

$(".question .title").on('click', function(){

	var question = $(this).parent();
	var height = question.find('p').height() + 40;

	// Close all
	$('.question').removeClass('open');
	$('.question .answer').css('height','0px');

	// Open clicked question
	question.addClass('open');
	question.find('.answer').css('height',height+'px');
});


// Google Maps

if( $('#map').length > 0 ){

	var map = new GMaps({
		div: '#map',
		lat: 54.5123151,
		lng: -6.0330270,
		streetViewControl: false,
		mapTypeControl: false,
		scrollwheel: false,
	});

	map.addMarker({
		lat: 54.5103220,
		lng: -6.0330270,
		//infoWindow: {
		//	content: '<div id="map-overlay"><h4>Transformation Centre (MenforGod)</h4><div class="icon marker"></div><p>137 Gregg Street <br> Lisburn, Northern Ireland <br> BT27 5AW</p><div class="arrow"></div></div>'
		//}
	});

	map.drawOverlay({
		lat: 54.5103220,
		lng: -6.0330270,
		layer: 'overlayLayer',
		content: '<div id="map-overlay"><h4>Transformation Centre</h4><div class="icon marker"></div><p>Gregg Street <br> Lisburn, Northern Ireland <br> BT27 5AW</p><div class="arrow"></div></div>'
	});

	// Disable map dragging on touch devices
	if ("ontouchstart" in document.documentElement){

		map.setOptions({draggable: false});
	}
}